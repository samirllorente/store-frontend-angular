import { Component, Inject, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-to-cart-modal',
  templateUrl: './add-to-cart-modal.component.html',
  styleUrls: ['./add-to-cart-modal.component.scss']
})
export class AddToCartModalComponent implements AfterViewInit {

  product: any;
  quantity: FormControl = new FormControl(1, Validators.required);

  constructor(
    public dialogRef: MatDialogRef<AddToCartModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.product = data;
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      document.getElementById('quantityInput').focus();
    }, 200);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  dataToSend(): any {
    return this.quantity.valid ? {
      product: this.product,
      quantity: this.quantity.value
    } : null;
  }

}
