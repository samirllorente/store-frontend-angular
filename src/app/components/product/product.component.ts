import { Component,  } from '@angular/core';
import { EntityServiceFactory, EntityService } from 'src/app/services/entity.service';
import { AddToCartModalComponent } from './add-to-cart-modal/add-to-cart-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent {

  idCart = null;
  cart: Array<any> = [];
  products: Array<any> = [];

  productService: EntityService;
  cartService: EntityService;

  total: any = {
    subtotal: 0,
    iva: 0,
    total: 0
  };
  iva = 19;
  price = 1000; // fixed price for all products

  constructor(
    private entityServiceFactory: EntityServiceFactory,
    private dialog: MatDialog,
    private snackBar: MatSnackBar)
  {
    this.productService = this.entityServiceFactory.loadEntity('/product');
    this.cartService = this.entityServiceFactory.loadEntity('/cart');
    this.productService.data$.subscribe(data => {
      this.products = data;
    });
    this.productService.getData(false);
    this.idCart = localStorage.getItem('idCart');
    if (this.idCart) {
      this.cartService.showOneData(this.idCart).then(data => {
        this.cart = data.products.map(x => {
          return {
            quantity: x.quantity,
            product: {
              id: x.product.id,
              name: x.product.name,
            }
          };
        });
        this.calculateTotal();
      });
    }
  }

  searchProducts(value): void {
    this.productService.search(value);
  }

  addToCartModal(product): void {
    const dialogRef = this.dialog.open(AddToCartModalComponent, {
      data: product
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const productPositionExisting = this.cart.map(x => x.product.id).indexOf(result.product.id);
        if (productPositionExisting !== -1) {
          this.cart[productPositionExisting].quantity += result.quantity;
        } else {
          this.cart.push(result);
        }
        this.updateCart();
        this.calculateTotal();
      }
    });
  }

  deleteCartProduct(product): void {
    this.cart = [...this.cart.filter(x => x.product.id !== product.product.id)];
    this.updateCart();
    this.calculateTotal();
  }

  generateData(): object {
    const data = {products: {}};
    const keys = this.cart.map(x => x.product.id);
    keys.forEach(key => {
      data.products[key] = {
        quantity: this.cart.find(x => x.product.id === key).quantity
      };
    });
    return data;
  }

  updateCart(): void {
    const data = this.generateData();
    if (this.idCart) {
      this.cartService.updateData(this.idCart, data);
    } else {
      this.cartService.saveData(data).then(response => {
        this.idCart = response.id;
        localStorage.setItem('idCart', this.idCart);
      });
    }
  }

  calculateTotal(): void {
    if (this.cart.length > 0) {
      const reducer = (accumulator, currentValue) => accumulator + currentValue;
      this.total.total = this.cart.map(x => this.price * x.quantity).reduce(reducer);
      this.total.iva = this.cart
        .map(x => {
          return (this.price * x.quantity * this.iva) / 100;
        })
        .reduce(reducer);
      this.total.subtotal = this.total.total - this.total.iva;
    } else {
      this.total = {
        subtotal: 0,
        iva: 0,
        total: 0
      };
    }
  }

  pay(): void {
    this.cartService.saveData(null, '/cart/pay/' + this.idCart).then(() =>{
      this.cart = [];
      this.idCart = null;
      this.calculateTotal();
      localStorage.removeItem('idCart');
      this.snackBar.open('El pago fue exitoso');
    });
  }

}
