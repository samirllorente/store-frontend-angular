import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EntityServiceFactory {
  constructor(private http: HttpClient) {}
  loadEntity(url: string): EntityService {
    return new EntityService(this.http).loadEntity(url);
  }
}

export class EntityService {

  url = null;
  filters: any = {
    limit: 10,
    page: 1
  };

  disabledNext = true;
  disabledPrev = true;

  originalData: any = null;
  subject$: Subject<any> = new Subject<any>();
  public data$: Observable<any> = this.subject$.asObservable();

  constructor(private http: HttpClient) { }

  loadEntity(url: string): EntityService {
    this.url = environment.backend + '/api' + url;
    return this;
  }

  getData(withoutFilter = true): Promise<any> {
    const promise = this.http.get(this.url + (withoutFilter ? '' : this.getParams())).toPromise();
    this.promiseToObservable(promise);
    return promise;
  }

  showOneData(id): Promise<any> {
    return this.http.get(this.url + '/' + id).toPromise();
  }

  saveData(body, url?): Promise<any> {
    return this.http.post(url ? (environment.backend + '/api' + url) : this.url, body).toPromise();
  }

  updateData(id, body): Promise<any> {
    return this.http.put(this.url + '/' + id, body).toPromise();
  }

  deleteData(id): Promise<any> {
    return this.http.delete(this.url + '/' + id).toPromise();
  }

  promiseToObservable(promise): any {
    promise
      .then(value => {
        this.originalData = JSON.parse(JSON.stringify(value));
        this.subject$.next(value.data ? value.data : value);
        this.updateVars();
      })
      .catch(reason => {
        this.subject$.error(reason);
      });
  }

  getParams(): string {
    const params = [];
    const keys = Object.keys(this.filters);
    keys.forEach(key => {
      params.push(key + '=' + this.filters[key]);
    });
    return '?' + params.join('&');
  }

  search(value): void {
    this.filters.page = 1;
    this.filters.q = value;
    this.getData(false);
  }

  next(): void {
    const pageLimit = parseInt(this.originalData.per_page, 10);
    if (this.filters.page < pageLimit) {
      this.filters.page++;
      this.getData(false);
    }
    this.updateVars();
  }

  prev(): void {
    if (this.filters.page > 1) {
      this.filters.page--;
      this.getData(false);
    }
    this.updateVars();
  }

  updateVars(): void {
    const pageLimit = parseInt(this.originalData.per_page, 10);
    this.disabledNext = this.filters.page === pageLimit;
    this.disabledPrev = this.filters.page === 1;
  }
}

